package org.hibernate.type;

/*
 * Hibernate, Relational Persistence for Idiomatic Java
 *
 * Copyright (c) 2010, Red Hat Inc. or third-party contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Red Hat Inc.
 *
 * This copyrighted material is made available to anyone wishing to use, modify,
 * copy, or redistribute it subject to the terms and conditions of the GNU
 * Lesser General Public License, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution; if not, write to:
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301  USA
 */

import org.hibernate.type.descriptor.ValueBinder;
import org.hibernate.type.descriptor.ValueExtractor;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.java.UUIDTypeDescriptor;
import org.hibernate.type.descriptor.sql.BasicBinder;
import org.hibernate.type.descriptor.sql.BasicExtractor;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

/**
 * Specialized type mapping for {@link UUID} and the Postgres UUID data type (which is mapped as OTHER in its
 * JDBC driver).
 *
 * @author Steve Ebersole
 * @author David Driscoll
 */
public class UUIDType extends AbstractSingleColumnStandardBasicType<UUID> {
    public static final String NAME = "uuid-name";
    private static final long serialVersionUID = 1475228955708142769L;

    public UUIDType() {
        super( UUIDSqlTypeDescriptor.INSTANCE, UUIDTypeDescriptor.INSTANCE );
    }

    public String getName() {
        return NAME;
    }

    public static class UUIDSqlTypeDescriptor implements SqlTypeDescriptor {
        public static final UUIDSqlTypeDescriptor INSTANCE = new UUIDSqlTypeDescriptor();
        private static final long serialVersionUID = -4956583145726385943L;

        @Override
        public int getSqlType() {
            // ugh
            return Types.VARCHAR;
        }

        @Override
        public boolean canBeRemapped() {
            return true;
        }

        @Override
        public <X> ValueBinder<X> getBinder( final JavaTypeDescriptor<X> javaTypeDescriptor ) {
            return new MyBasicBinder<>( javaTypeDescriptor, this );
        }

        @Override
        public <X> ValueExtractor<X> getExtractor( final JavaTypeDescriptor<X> javaTypeDescriptor ) {
            return new MyBasicExtractor<>( javaTypeDescriptor, this );
        }

        private static final class MyBasicBinder<X> extends BasicBinder<X> {
            private final JavaTypeDescriptor<X> javaTypeDescriptor;

            private MyBasicBinder(
                    final JavaTypeDescriptor<X> javaTypeDescriptor,
                    final UUIDSqlTypeDescriptor typeDescriptor ) {
                super( javaTypeDescriptor, typeDescriptor );
                this.javaTypeDescriptor = javaTypeDescriptor;
            }

            @Override
            protected void doBind(
                    final PreparedStatement st,
                    final X value,
                    final int index,
                    final WrapperOptions options )
                    throws SQLException {
                st.setObject( index, javaTypeDescriptor.unwrap( value, UUID.class, options ) );
            }
        }

        private static final class MyBasicExtractor<X> extends BasicExtractor<X> {
            private final JavaTypeDescriptor<X> javaTypeDescriptor;

            private MyBasicExtractor(
                    final JavaTypeDescriptor<X> javaTypeDescriptor,
                    final UUIDSqlTypeDescriptor uuidSqlTypeDescriptor
            ) {
                super( javaTypeDescriptor, uuidSqlTypeDescriptor );
                this.javaTypeDescriptor = javaTypeDescriptor;
            }

            @Override
            protected X doExtract(
                    final ResultSet rs,
                    final String name,
                    final WrapperOptions options )
                    throws SQLException {
                return javaTypeDescriptor.wrap( rs.getObject( name ), options );
            }

            @Override
            protected X doExtract(
                    final CallableStatement statement,
                    final int index,
                    final WrapperOptions options )
                    throws SQLException {
                return javaTypeDescriptor.wrap( statement.getObject( index ), options );
            }

            @Override
            protected X doExtract(
                    final CallableStatement statement,
                    final String name,
                    final WrapperOptions options )
                    throws SQLException {
                return javaTypeDescriptor.wrap( statement.getObject( name ), options );
            }
        }
    }
}

