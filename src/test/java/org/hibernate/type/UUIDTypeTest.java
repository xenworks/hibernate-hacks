package org.hibernate.type;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class UUIDTypeTest {

    @Test
    public void testGetName() throws Exception {
        UUIDType uuidType0 = new UUIDType();

        assertThat( uuidType0, instanceOf( SingleColumnType.class ) );

        assertThat( uuidType0.getName(), equalTo( UUIDType.NAME ) );
    }
}
